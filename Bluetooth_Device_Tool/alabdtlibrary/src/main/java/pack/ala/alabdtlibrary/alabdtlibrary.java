package pack.ala.alabdtlibrary;

import android.content.Context;
import android.content.Intent;

public class alabdtlibrary {
    private String hello;
    public void setHello(String hello){
        this.hello=hello;
    }
    public String getHello(){
        return this.hello;
    }
    public void startalabdtlibrary(Context mcontext){
        Intent scannerView = new Intent();
        scannerView.setClass(mcontext, pack.ala.alabdtlibrary.bluetooth_device_tool.alabdtlibrary_MainActivity.class);
        scannerView.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mcontext.startActivity(scannerView);
    }
}
