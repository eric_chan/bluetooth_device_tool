package pack.ala.alabdtlibrary.ala_ble;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.util.List;
import java.util.UUID;

public class BluetoothAlaService extends Service {
    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    public BluetoothGatt mBluetoothGatt;
    private final IBinder mBinder = new LocalBinder();
    public final static String ACTION_GATT_CONNECTED = "ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED = "ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED = "ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE = "ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA = "EXTRA_DATA";

    public static final UUID UUID_NOTIFY = UUID.fromString("6E400003-B5A3-F393-E0A9-E50E24DCCA9E");
    public static final UUID UUID_WRITE = UUID.fromString("6E400002-B5A3-F393-E0A9-E50E24DCCA9E");
    public static final UUID UUID_CLIENT_CHARACTERISTIC_CONFIG = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    public static final UUID UUID_HEART_RATE_MEASUREMENT = UUID.fromString("00002a37-0000-1000-8000-00805f9b34fb");
    public static UUID UUID_TESTDEVICE = null;
//    public final static UUID UUID_CLIENT_CHARACTERISTIC_CONFIG = UUID.fromString("6E40FD03-B5A3-F393-E0A9-E50E24DCCA9E");

    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Log.v(getPackageName(),"ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        BluetoothLeService  ^  onConnectionStateChange\nstatus : "+status+"\nnewState : "+newState+"\n");
            switch (newState) {
                case BluetoothProfile.STATE_CONNECTED:
                    broadcastUpdate(ACTION_GATT_CONNECTED);
                    mBluetoothGatt.discoverServices();
                    break;
                case BluetoothProfile.STATE_DISCONNECTED:
                    broadcastUpdate(ACTION_GATT_DISCONNECTED);
                    break;
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            Log.v(getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        BluetoothLeService  ^  onServicesDiscovered received: " + status);
            if (status == BluetoothGatt.GATT_SUCCESS)
                broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            Log.v(getPackageName(),"ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        BluetoothLeService  ^  onCharacteristicRead");
            if (status == BluetoothGatt.GATT_SUCCESS)
                broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            Log.v(getPackageName(),"ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        BluetoothLeService  ^  onCharacteristicChanged");
            Log.v(getPackageName(),"ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        BluetoothLeService  ^  characteristic  "+characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0));
            broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
        }
    };

    //-----------------------------------------------------------------------------------------------------------
    private void broadcastUpdate(final String action) {
        sendBroadcast(new Intent(action));
    }

    //-----------------------------------------------------------------------------------------------------------
    private void broadcastUpdate(final String action, final BluetoothGattCharacteristic characteristic) {
        Log.v(getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        BluetoothLeService  ^  broadcastUpdate  "+characteristic.toString());
        final Intent intent = new Intent(action);
        if (UUID_NOTIFY.equals(characteristic.getUuid()) || UUID_WRITE.equals(characteristic.getUuid())) {
            Log.v(getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        BluetoothLeService  ^  broadcastUpdate  「WB」" );
            Log.d(characteristic.getUuid().equals(UUID_NOTIFY) ? "broadcastUpdate_notify" : "broadcastUpdate_write", characteristic.getUuid() + "");
//            String string="<WB>";
//            for(int i = 0; i < 20; i++)
//                string+=characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, i)+",";
//            intent.putExtra( EXTRA_DATA, string);
            final byte[] data = characteristic.getValue();
            if (data != null && data.length > 0) {
                final StringBuilder stringBuilder = new StringBuilder(data.length);
                for (byte byteChar : data)
                    stringBuilder.append(String.format("%02X,", byteChar));
                intent.putExtra(EXTRA_DATA, stringBuilder.toString().substring(0, stringBuilder.length() - 1));
            }
        }else{
            Log.v(getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        BluetoothLeService  ^  broadcastUpdate  「Other」");
            final byte[] data = characteristic.getValue();
            Log.v(getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        BluetoothLeService  ^  broadcastUpdate  data  "+data.toString());
            if (data != null && data.length > 0) {
                final StringBuilder stringBuilder = new StringBuilder(data.length);
                for (byte byteChar : data)
                    stringBuilder.append(String.format("%02X,", byteChar));
                intent.putExtra(EXTRA_DATA, stringBuilder.toString().substring(0, stringBuilder.length() - 1));
            }
        }
        sendBroadcast(intent);
    }

    //-----------------------------------------------------------------------------------------------------------
    public class LocalBinder extends Binder {
        public BluetoothAlaService getService() {
            return BluetoothAlaService.this;
        }
    }

    //-----------------------------------------------------------------------------------------------------------
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    //-----------------------------------------------------------------------------------------------------------
    @Override
    public boolean onUnbind(Intent intent) {
        close();
        return super.onUnbind(intent);
    }

    //-----------------------------------------------------------------------------------------------------------
    public boolean initialize() {
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null){
                Log.v(getPackageName(),"ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        BluetoothLeService  ^  initialize mBluetoothManager null");
                return false;
            }

        }
        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null){
            Log.v(getPackageName(),"ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        BluetoothLeService  ^  initialize mBluetoothAdapter null");
            return false;
        }

        return true;
    }

    //-----------------------------------------------------------------------------------------------------------
    public boolean connect(final String address) {
        if (mBluetoothAdapter == null || address == null)
            return false;

        Log.v(getPackageName(),"ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        BluetoothLeService  ^  connect "+address);

        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null)
            return false;
        // Previously connected device.  Try to reconnect.
        if (mBluetoothDeviceAddress != null && address.equals(mBluetoothDeviceAddress) && mBluetoothGatt != null) {
            if (mBluetoothGatt.connect()){
                return true;
            }else{
                if (device != null){
                    initialize();
                }else{
                    return false;
                }
            }
        }
        mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
        mBluetoothDeviceAddress = address;
        return true;
    }

    //-----------------------------------------------------------------------------------------------------------
    public void disconnect() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null)
            return;
        mBluetoothGatt.disconnect();
    }

    //-----------------------------------------------------------------------------------------------------------
    public void close() {
        if (mBluetoothGatt == null)
            return;
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    //-----------------------------------------------------------------------------------------------------------
    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        Log.v(getPackageName(),"ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        BluetoothLeService  ^  BluetoothGattCharacteristic "+characteristic.getUuid());
        if (mBluetoothAdapter == null || mBluetoothGatt == null){
            Log.v(getPackageName(),"ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        BluetoothLeService  ^  readCharacteristic null");
            return;
        }

        mBluetoothGatt.readCharacteristic(characteristic);
    }

    //-----------------------------------------------------------------------------------------------------------
    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic, boolean enabled) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null){
            Log.v(getPackageName(),"ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        BluetoothLeService  ^  setCharacteristicNotification   null");
            return;
        }
        mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);
        BluetoothGattDescriptor descriptor = null;
//        if (UUID_NOTIFY.equals(characteristic.getUuid()) || UUID_WRITE.equals(characteristic.getUuid())) {
//            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(UUID_CLIENT_CHARACTERISTIC_CONFIG);
//            if (descriptor==null)   return;
//            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
//            mBluetoothGatt.writeDescriptor(descriptor);
//        }
        if(UUID_TESTDEVICE==null){
            descriptor = characteristic.getDescriptor(UUID_CLIENT_CHARACTERISTIC_CONFIG);
            Log.v(getPackageName(),"ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        BluetoothLeService  ^  setCharacteristicNotification   UUID_CLIENT_CHARACTERISTIC_CONFIG  "+UUID_CLIENT_CHARACTERISTIC_CONFIG);
        }else{
            descriptor = characteristic.getDescriptor(UUID_TESTDEVICE);
            Log.v(getPackageName(),"ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        BluetoothLeService  ^  setCharacteristicNotification   UUID_TESTDEVICE  "+UUID_TESTDEVICE);
        }
        if (descriptor == null) return;
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        mBluetoothGatt.writeDescriptor(descriptor);
    }
    //-----------------------------------------------------------------------------------------------------------
    public List<BluetoothGattService> getSupportedGattServices() {
        if (mBluetoothGatt == null)
            return null;
        return mBluetoothGatt.getServices();
    }

    public byte[] arr_data_V2 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    public void writeCharacteristicPage_v20(BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null)
            return;

        characteristic.setValue(arr_data_V2);
        mBluetoothGatt.writeCharacteristic(characteristic);
    }
}
