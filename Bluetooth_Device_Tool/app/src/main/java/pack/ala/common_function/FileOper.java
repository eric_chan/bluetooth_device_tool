package pack.ala.common_function;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.google.gson.Gson;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Properties;

//import common_httpjson.StringConverter;


public class FileOper //extends View 
{

    String xMLString;

    public String[] str_fileName;
    public int i_fileCnt = 0;
    public int i_ListCnt = 2;// 多一行標題 //如i_fileCnt=0 還要多一行 No file

    public String str_sensor_uuid = "";
    public boolean b_sensor_enable = true;

    public boolean b_haveInfo = false;
    public int u8_Type = 0;

    public int i_maxSpd = 0;
    public int i_minSpd = 0;

    public int i_maxLev = 0;
    public int i_minLev = 0;

    public int i_LevNo = 0; //階數
    public int u8_stageNum = 0;
    public int u8_stageGrowMode = 0;
    public int u8_unit = 0;
    public int u8_appProgram = 0;

//	StringConverter mconxml;

    public FileOper() {
        i_fileCnt = 0;
        i_ListCnt = 2;// 多一行標題 //如i_fileCnt=0 還要多一行 No file
    }

    //----------------------------------------------------------------------------------------
    public void makeDir(Context context, String name) {
        File file = context.getFilesDir();
        String path = file.getAbsolutePath();
        String pathx = path + "/" + name;
        File filex = new File(pathx);
        boolean is = filex.exists();// 判断文件（夹）是否存在

        if (!is) {
            filex.mkdir();// 创建文件夹
        }
    }

    //----------------------------------------------------------------------------------------
    public void makeDir(String name) {
        String pathName = "/0";

        File rootPath = Environment.getExternalStorageDirectory();
        File file = new File(rootPath.getParent() + pathName);// 這邊是實際手機放置音樂的資料夾
        String path = file.getAbsolutePath();
        String pathx = path + "/" + name;
        File filex = new File(pathx);
        boolean is = filex.exists();// 判断文件（夹）是否存在

        //Log.v("csv","DIR"+pathx);

        if (!is) {
            filex.mkdir();// 创建文件夹
        }
    }


    // ---------------------------------------------------------------------------------------------
    public void fileDelete(String fileName, String folder) {
        String pathName = "/0";

        File rootPath = Environment.getExternalStorageDirectory();
        File file = new File(rootPath.getParent() + pathName);// 這邊是實際手機放置音樂的資料夾
        String path = file.getAbsolutePath();
        String pathx = path + "/" + folder + "/" + fileName;
        File filex = new File(pathx);

        if (filex.exists()) {
            filex.delete();
        }
    }

    //----------------------------------------------------------------------------------------
    //Check 檔案 是否存在	
    public boolean fileExist(Context context, String fileName, String folder) {
        //Context context=this;//首先，在Activity里获取context
        File file = context.getFilesDir();
        String path = file.getAbsolutePath();
        String pathx = path + "/" + folder + "/" + fileName;
        File filex = new File(pathx);
        return filex.exists();
    }

    //----------------------------------------------------------------------------------------
    //Check 檔案 是否存在	
    public boolean fileExist(String fileName, String folder) {
        String pathName = "/0";

        File rootPath = Environment.getExternalStorageDirectory();
        File file = new File(rootPath.getParent() + pathName);// 這邊是實際手機放置音樂的資料夾
        String path = file.getAbsolutePath();
        String pathx = path + "/" + folder + "/" + fileName;
        File filex = new File(pathx);

        return filex.exists();
    }
    //----------------------------------------------------------------------------------------

    public String getFilePath(Context context, String fileName, String folder) {
        //Context context=this;//首先，在Activity里获取context
        File file = context.getFilesDir();
        String path = file.getAbsolutePath();
        String pathx = path + "/" + folder + "/" + fileName;
        //File filex=new File(pathx);
        return pathx;//filex.exists();
    }

    //----------------------------------------------------------------------------------------
    public File getFile(Context context, String fileName, String folder) {
        //Context context=this;//首先，在Activity里获取context
        File file = context.getFilesDir();
        String path = file.getAbsolutePath();
        String pathx = path + "/" + folder + "/" + fileName;
        File filex = new File(pathx);
        return filex;//filex.exists();
    }

    //----------------------------------------------------------------------------------------
    public String getFilePath(String fileName, String folder) {
        String pathName = "/0";

        File rootPath = Environment.getExternalStorageDirectory();
        File file = new File(rootPath.getParent() + pathName);// 這邊是實際手機放置音樂的資料夾
        String path = file.getAbsolutePath();
        String pathx = path + "/" + folder + "/" + fileName;
        return pathx;
        //File filex = new File(pathx);

        //return filex.exists();
    }


    //----------------------------------------------------------------------------------------

    public void listFile(Context context, String folder) // 檔案列表
    {

        File filepath = context.getFilesDir();
        String path = filepath.getAbsolutePath();
        String pathx = path + "/" + folder;
        File f = new File(pathx);
        File file[] = f.listFiles();

        str_fileName = new String[file.length];

        for (int i = 0; i < file.length; i++) {
            str_fileName[i] = (file[i].getName());
        }

        i_fileCnt = file.length;

        if (i_fileCnt == 0) {
            i_ListCnt = 2;// Title + No file
        } else {
            i_ListCnt = i_fileCnt + 1;// Title + 檔案
        }

        Sort_file();
    }

    // ---------------------------------------------------------------------------------------------
    private void Sort_file() // 字串排序
    {
        String str_filename_temp = new String();
        String str_StringA = new String();
        String str_StringB = new String();

        for (int iCnt = 0; iCnt < str_fileName.length; iCnt++) {
            for (int jCnt = 0; jCnt < str_fileName.length - iCnt - 1; jCnt++) {
                str_StringA = str_fileName[jCnt].toLowerCase(Locale
                        .getDefault());
                str_StringB = str_fileName[jCnt + 1].toLowerCase(Locale
                        .getDefault());

                if (str_StringA.compareTo(str_StringB) < 0) {
                    str_filename_temp = str_fileName[jCnt];
                    str_fileName[jCnt] = str_fileName[jCnt + 1];
                    str_fileName[jCnt + 1] = str_filename_temp;
                }
            }

        }
    }

    // ---------------------------------------------------------------------------------------------

    public void readPropertiesFile(Context context, String fileName, String folder) // 讀取 key value 檔案
    {
        str_sensor_uuid = "";
        b_sensor_enable = false;

        File file = context.getFilesDir();
        String path = file.getAbsolutePath();
        String pathx = path + "/" + folder + "/" + fileName;
        Properties prop = new Properties();
        prop = loadConfig(context, pathx);

        str_sensor_uuid = (prop.getProperty("str_sensor_uuid"));
        b_sensor_enable = Boolean.parseBoolean(prop.getProperty("b_sensor_enable"));
    }

    // ---------------------------------------------------------------------------------------------
    public void savePropertiesFile(Context context, String fileName, String folder) // 存key Value 檔案
    {
        File file = context.getFilesDir();
        String path = file.getAbsolutePath();
        String pathx = path + "/" + folder + "/" + fileName;
        Properties prop = new Properties();
        prop.put("str_sensor_uuid", str_sensor_uuid);
        prop.put("b_sensor_enable", Boolean.toString(b_sensor_enable));
        saveConfig(context, pathx, prop);
    }

    // ---------------------------------------------------------------------------------------------
    public Properties loadConfig(Context context, String file) {
        Properties properties = null;
        try {
            File f = new File(file);
            if (f.exists()) {
                FileInputStream fis = new FileInputStream(file);
                properties = new Properties();
                properties.load(fis);
            }
        } catch (Exception e) {
            Log.e(e.getMessage(), e.getMessage());
        }
        return properties;
    }

// ---------------------------------------------------------------------------------------------

	/*
	//CSV//
	-(void)saveCSVFile:(NSString *)str_FileName Folder:(NSString *)str_FolderName title:(NSString *)str_title colCount:(int)i_column dataArray:(NSMutableArray *)marr_data
	{
	    
	    if(marr_data.count<=0)
	    {
	        return;
	    }
	    
	    if(i_column<1)
	    {
	        i_column=1;
	    }
	   // NSArray *paths;
	    NSString *Docpaths=[self returnFilePath:str_FileName Folder:str_FolderName];
	    
	    int i_row=(int)marr_data.count/i_column;
	    
	    if (i_row*i_column!=marr_data.count)
	    {
	        i_row++;
	    }
	    
	    if (![[NSFileManager defaultManager] fileExistsAtPath:Docpaths])
	    {
	        [[NSFileManager defaultManager] createFileAtPath: Docpaths contents:nil attributes:nil];
	        //NSLog(@"Route creato");
	    }
	    
	    NSMutableString *writeString = [NSMutableString stringWithCapacity:0];
	    
	    if(str_title!=nil)
	    {
	        [writeString appendString:[NSString stringWithFormat:@"%@\n",str_title]];
	    }
	    
	    for (int i_rowCnt=0; i_rowCnt<i_row; i_rowCnt++)
	    {
	        NSMutableString *rowString = [NSMutableString stringWithCapacity:0];
	        
	        for(int i_dataCnt=i_column*i_rowCnt;i_dataCnt<i_column*(i_rowCnt+1);i_dataCnt++)
	        {
	            [rowString appendString:[NSString stringWithFormat:@"%@,",[marr_data objectAtIndex:i_dataCnt]]];
	        }
	        
	        [rowString appendString:[NSString stringWithFormat:@"\n"]];
	        
	        [writeString appendString:[NSString stringWithFormat:@"%@",rowString]];
	    }
	    

	    //Moved this stuff out of the loop so that you write the complete string once and only once.
	    //NSLog(@"writeString :%@",writeString);

	    NSFileHandle *handle;
	    handle = [NSFileHandle fileHandleForWritingAtPath: Docpaths];
	    //say to handle where's the file fo write
	    [handle truncateFileAtOffset:[handle seekToEndOfFile]];
	    //position handle cursor to the end of file
	    [handle writeData:[writeString dataUsingEncoding:NSUTF8StringEncoding]];


	    
	  //[NSLog(@"")]
	    
	    
	}


*/

    public void saveCSVFile(Context context, String fileName, String folder, String str_title, int i_column, ArrayList<?> marr_data)//((NSString *)str_FileName Folder:(NSString *)str_FolderName title:(NSString *)str_title colCount:(int)i_column dataArray:(NSMutableArray *)marr_data
    {

        String pathName = "/0";

        File rootPath = Environment.getExternalStorageDirectory();
        File file = new File(rootPath.getParent() + pathName);// 這邊是實際手機放置音樂的資料夾
        String path = file.getAbsolutePath();
        String pathx = path + "/" + folder + "/" + fileName;

        String SPLIT = ",";
        String ENDCHAR = "\r\n";

        if (marr_data.size() <= 0) {
            return;
        }

        if (i_column < 1) {
            i_column = 1;
        }
        // NSArray *paths;
        //NSString *Docpaths=[self returnFilePath:str_FileName Folder:str_FolderName];

        int i_row = (int) marr_data.size() / i_column;

        if (i_row * i_column != marr_data.size()) {
            i_row++;
        }


        FileWriter fw = null;
        try {


            fw = new FileWriter(pathx);


            if (str_title != null) {
                fw.write(str_title + SPLIT + ENDCHAR);
            }

            for (int i_rowCnt = 0; i_rowCnt < i_row; i_rowCnt++) {
                for (int i_dataCnt = i_column * i_rowCnt; i_dataCnt < i_column * (i_rowCnt + 1); i_dataCnt++) {
                    fw.write((marr_data.get(i_dataCnt)).toString() + SPLIT);
                }

                fw.write(ENDCHAR);
            }


            //============
            fw.close();
//	            Log.v("csv", "FileClose");
        } catch (IOException e) {
            // TODO Auto-generated method stub
            e.printStackTrace();
        }


    }

    //20150512
    // ---------------------------------------------------------------------------------------------
    public void saveXMLFile(Context context, String fileName, String folder, String str_title, int i_column, ArrayList<?> marr_data)//((NSString *)str_FileName Folder:(NSString *)str_FolderName title:(NSString *)str_title colCount:(int)i_column dataArray:(NSMutableArray *)marr_data
    {
        String pathName = "/0";

        File rootPath = Environment.getExternalStorageDirectory();
        File file = new File(rootPath.getParent() + pathName);// 這邊是實際手機放置音樂的資料夾
        String path = file.getAbsolutePath();
        String pathx = path + "/" + folder + "/" + fileName;
        String SPLIT = ",";
        String ENDCHAR = "\r\n";

        if (marr_data.size() <= 0) {
            return;
        }

        FileWriter fw = null;
        try {
//	        	if(folder.equals("ala_lsit_xml") || folder.equals("ala_note_xml")) {
//		            fw = new FileWriter(pathx);
//	//	            for (int icnt = 0; icnt < xMLString.length(); icnt++)
//	//				{
//		            xMLString = mconxml.SportXMLDoc(marr_data);
//			        fw.write(xMLString);
////			        Log.v("xMLString",xMLString);
//	        	}else if(folder.equals("ala_gps_xml")){
//	        		fw = new FileWriter(pathx);
//	        		xMLString = mconxml.CreateGPSXMLDoc(marr_data);
//			        fw.write(xMLString);
////			        Log.v("xMLString",xMLString);
//				}else {
//					fw = new FileWriter(pathx);
//	        		xMLString = mconxml.CreateXMLDoc(marr_data);
//			        fw.write(xMLString);
//				}

            //============
            fw.close();
//	            Log.v("xml", "FileClose");
        } catch (IOException e) {
            // TODO Auto-generated method stub
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    //20150427
    // ---------------------------------------------------------------------------------------------
    public void saveJSONFile(Context context, ArrayList<?> marr_data)//((NSString *)str_FileName Folder:(NSString *)str_FolderName title:(NSString *)str_title colCount:(int)i_column dataArray:(NSMutableArray *)marr_data
    {
        String pathName = "/0";

//			File rootPath = Environment.getExternalStorageDirectory();
//			File file = new File(rootPath.getParent() + pathName);// 這邊是實際手機放置音樂的資料夾
//			String path = file.getAbsolutePath();
//			String pathx = path + "/" + folder + "/" + fileName;
//					
//		    String SPLIT = ",";  
//		    String ENDCHAR = "\r\n";  

        if (marr_data.size() <= 0) {
            return;
        }

//		    for (int iCnt =0; iCnt < marr_data.size(); iCnt++)
//		    {
//		    	JSONObject jso = new JSONObject();
        Gson gson = new Gson();
//		   	 	String srcPath = Environment.getExternalStorageDirectory().getPath()+"/file.json";

        // convert java object to JSON format,and returned as JSON formatted string
        String json = gson.toJson(marr_data);

//		    	try {
//		    		//write converted json data to a file named "file.json"
//		    		FileWriter writer = new FileWriter(pathx);
//		    		writer.write(json);
//		    		writer.close();
//		     
//		    	} catch (IOException e) {
//		    		e.printStackTrace();
//		    	}

//		    	Log.w("Gson", json);
//		    }

    }
	/* //20140922
	// ---------------------------------------------------------------------------------------------
	public void savePropertiesFileBTM(Context context,String fileName, String folder) // 存key Value 檔案
	{
		File file = context.getFilesDir();
		String path = file.getAbsolutePath();
		String pathx = path + "/" + folder + "/" + fileName;
		Properties prop = new Properties();
		prop.put("str_sensor_uuid", str_sensor_uuid);
		prop.put("b_sensor_enable", Boolean.toString(b_sensor_enable));
		
		
		
		
		
		
		saveConfig(context, pathx, prop);
	}
	 */
    // ---------------------------------------------------------------------------------------------


    public void saveConfig(Context context, String file, Properties properties) {
        try {
            FileOutputStream fos = new FileOutputStream(file, false);
            properties.store(fos, "store");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // ---------------------------------------------------------------------------------------------
    public void fileDelete(Context context, String fileName, String folder) {
        File file = context.getFilesDir();
        String path = file.getAbsolutePath();
        String pathx = path + "/" + folder + "/" + fileName;
        File filex = new File(pathx);

        if (filex.exists()) {
            filex.delete();
        }
    }

    // ---------------------------------------------------------------------------------------------
    public boolean checkSensorEnable(Context context, String folder) // 確認是否有任何Sensor被開啟 全部沒有則要把外面的也關掉
    {
        listFile(context, folder); // 檔案列表

        for (String str_file : str_fileName) {
            readPropertiesFile(context, str_file, folder);

            if (b_sensor_enable == true) {
                return true;
            }
        }
        return false;
    }

    public boolean copyFile(String oldPath, String newPath) {
        boolean isok = true;
        try {
            //int bytesum = 0 ;
            int byteread = 0;
            File oldfile = new File(oldPath);
            if (oldfile.exists()) { //檔案存在時
                InputStream inStream = new FileInputStream(oldPath); //讀入原檔案
                FileOutputStream fs = new FileOutputStream(newPath);
                byte[] buffer = new byte[1024];
                //int length;
                while ((byteread = inStream.read(buffer)) != -1) {
                    //bytesum += byteread; //byte數檔案大小
                    //System.out.println(bytesum);
                    fs.write(buffer, 0, byteread);
                }
                fs.flush();
                fs.close();
                inStream.close();
            } else {
                isok = false;
            }
        } catch (Exception e) {
            // System.out.println("複製單個檔案操作出錯");
            // e.printStackTrace();
            isok = false;
        }
        return isok;

    }
    // ---------------------------------------------------------------------------------------------
	/*
	public void copy(File src, File dst) throws IOException 
	{
	    InputStream in = new FileInputStream(src);
	    OutputStream out = new FileOutputStream(dst);

	    // Transfer bytes from in to out
	    byte[] buf = new byte[1024];
	    int len;
	    while ((len = in.read(buf)) > 0) {
	        out.write(buf, 0, len);
	    }
	    in.close();
	    out.close();
	}
	*/
    //-------------------------------------------------------------------------------------------------
	/*
	public void writeListToFile(ArrayList<?> mArrayList,Context context)
	{
		File file = context.getFilesDir();
		String path = file.getAbsolutePath();
		String pathx = path + "/output";
		try 
		{
		    FileOutputStream fos = new FileOutputStream(pathx);
		    ObjectOutputStream oos = new ObjectOutputStream(fos);   
		    oos.writeObject(mArrayList); // write MenuArray to ObjectOutputStream
		    oos.close(); 
		    Log.v("test","OK");
		} 
		catch(Exception ex) 
		{
		    ex.printStackTrace();
		    Log.v("test","Fail");
		}
	}
	
// ---------------------------------------------------------------------------------------------	

	public void readListToFile(ArrayList<?> mArrayList,Context context)
	{
		File file = context.getFilesDir();
		String path = file.getAbsolutePath();
		String pathx = path + "/output";
		
		try 
		{
		    FileInputStream fis = new FileInputStream(pathx);
		    ObjectInputStream ois = new ObjectInputStream(fis);   
		    mArrayList=(ArrayList<?>)ois.readObject();//readObject();
		    ois.close(); 
		    Log.v("test","OK2");
		    
		    

		      
		}
		catch(Exception ex) 
		{
		    ex.printStackTrace();
		    Log.v("test","OK");
		}

	}
	*/

    // ---------------------------------------------------------------------------------------------
    //20140922
    public void savePropertiesFileBTM(Context context, String fileName, String folder) // 存key Value 檔案
    {
        File file = context.getFilesDir();
        String path = file.getAbsolutePath();
        String pathx = path + "/" + folder + "/" + fileName;
        Properties prop = new Properties();
        prop.put("str_sensor_uuid", str_sensor_uuid);
        prop.put("b_sensor_enable", Boolean.toString(b_sensor_enable));
        //Log.v("Boolean","Boolean.toString(b_sensor_enable):"+Boolean.toString(b_sensor_enable));


        if (b_haveInfo == true) {
            prop.put("b_haveInfo", Boolean.toString(b_haveInfo));
            prop.put("u8_Type", Integer.toString(u8_Type));
            prop.put("i_maxSpd", Integer.toString(i_maxSpd));
            prop.put("i_minSpd", Integer.toString(i_minSpd));
            prop.put("i_maxLev", Integer.toString(i_maxLev));
            prop.put("i_minLev", Integer.toString(i_minLev));
            prop.put("i_LevNo", Integer.toString(i_LevNo));
        }

        saveConfig(context, pathx, prop);
    }


    // ---------------------------------------------------------------------------------------------
    //20140922
    public void readPropertiesFileBTM(Context context, String fileName, String folder) // 讀取 key value 檔案
    {
        str_sensor_uuid = "";
        b_sensor_enable = false;

        File file = context.getFilesDir();
        String path = file.getAbsolutePath();
        String pathx = path + "/" + folder + "/" + fileName;
        Properties prop = new Properties();
        prop = loadConfig(context, pathx);

        str_sensor_uuid = (prop.getProperty("str_sensor_uuid"));
        b_sensor_enable = Boolean.parseBoolean(prop.getProperty("b_sensor_enable"));

        b_haveInfo = Boolean.parseBoolean(prop.getProperty("b_haveInfo", "false"));


        if (b_haveInfo == true) {
            u8_Type = Integer.parseInt(prop.getProperty("u8_Type"));
            i_maxSpd = Integer.parseInt(prop.getProperty("i_maxSpd"));
            i_minSpd = Integer.parseInt(prop.getProperty("i_minSpd"));
            i_maxLev = Integer.parseInt(prop.getProperty("i_maxLev"));
            i_minLev = Integer.parseInt(prop.getProperty("i_minLev"));
            i_LevNo = Integer.parseInt(prop.getProperty("i_LevNo"));
			
			/*
			Log.v("Fitness","u8_Type:"+u8_Type);
			Log.v("Fitness","i_maxSpd:"+i_maxSpd);
			Log.v("Fitness","i_minSpd:"+i_minSpd);
			Log.v("Fitness","i_maxLev:"+i_maxLev);
			Log.v("Fitness","i_minLev:"+i_minLev);
			Log.v("Fitness","i_LevNo:"+i_LevNo);
			*/
        }

    }

    // ---------------------------------------------------------------------------------------------

}
