package pack.ala.alabdtlibrary.bluetooth_device_tool;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.io.File;
import java.util.ArrayList;

import pack.ala.alabdtlibrary.R;

public class alabdtlibrary_MainActivity extends AppCompatActivity {
    public static final String FILE_ALA_BDT = Environment.getExternalStorageDirectory().getPath() + "/ALA_BDT/";
    public static final String FILE_ALA_ES = Environment.getExternalStorageDirectory().getPath() + "/ALA_BDT/ES/";
    public static SharedPreferences sharedPreferences;
    private static String[] PERMISSIONS_ALA_CONNECT = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_COARSE_LOCATION};
    private static final int REQUEST_EXTERNAL_ALA_CONNECT = 777;
    private Boolean canUse = false;
    private TextView connectDeviceStart;
    private TextView testTextView;
    private TextView ScanDeviceSetting;
    private TextView connectDeviceVar;
    public static String ScanDeviceString = "";
    public static ArrayList<String> SCAN_DEVICE = new ArrayList<String>();
    public static ArrayList<String> FILE_OBJ = new ArrayList<String>();
    public File fileES;
    public static String adddefault = "default";
    public static String addHRMsensor = "HRM sensor";
    public static String addStarONE = "Star ONE";
    public static String addOD007 = "OD007";
    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.alabdtlibrary_start);
        sharedPreferences = getSharedPreferences("Ala_bluetooth_device_tool", 0);
        ScanDeviceString = sharedPreferences.getString("SCAN_DEVICE", "Add…_BDTS_");
//        ScanDeviceString = "Add…_BDTS_"+adddefault+"_BDTS_";
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        connectDeviceStart = (TextView) findViewById(R.id.connectDeviceStart);
        testTextView = (TextView) findViewById(R.id.testTextView);
        ScanDeviceSetting = (TextView) findViewById(R.id.ScanDeviceSetting);
        connectDeviceVar = (TextView) findViewById(R.id.connectDeviceVar);
        connectDeviceStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ScanDeviceString.trim().equals("Add…_BDTS_")) {
                    settingScan();
                } else {
                    startAPP();
                }
            }
        });
        testTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        testTextView.setVisibility(View.GONE);
        ScanDeviceSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settingScan();
            }
        });
        connectDeviceVar.setText("Version：" + pInfo.versionName);
        initPermission();
        initScanDevice(ScanDeviceString);
        getAngle();
    }
    double x = -0.21307299961409973;
    double y = 0.1338949997575004;
    double z = 0.990944998205282;
    private void getAngle(){
        Log.w("Ⓜⓐⓣⓣⓔⓛ", "Ⓜⓐⓣⓣⓔⓛ        原始Ｇ值 " + x+" , "+ y+" , "+ z);
        double θx = Math.atan2(Math.sqrt(1-Math.pow(x,2)),x)*(180/Math.PI);
        double θy = Math.atan2(Math.sqrt(1-Math.pow(y,2)),y)*(180/Math.PI);
        double θz = Math.atan2(Math.sqrt(1-Math.pow(z,2)),z)*(180/Math.PI);
        Log.e("Ⓜⓐⓣⓣⓔⓛ", "Ⓜⓐⓣⓣⓔⓛ        原始角度 " + (θx-0)%360+" , "+ (θy-0)%360+" , "+ (θz+0)%360 +"\n\n");
        for (int i=0;i<8;i++){
            setAngle(0,-45,0);
        }
    }
    private void setAngle(double θx,double θy,double θz){
        double φx = Math.toRadians(θx);
        double φy = Math.toRadians(θy);
        double φz = Math.toRadians(θz);
//        double xT = x*(Math.cos(φx)+(Math.pow(x,2)*(1-Math.cos(φx))))+
//                y*(((x*y)*(1-Math.cos(φx))-(z*Math.sin(φx))))+
//                z*(((x*z)*(1-Math.cos(φx))-(y*Math.sin(φx))));
//        double yT = x*(((x*y)*(1-Math.cos(φy))+(z*Math.sin(φy))))+
//                y*(Math.cos(φy)+(Math.pow(y,2)*(1-Math.cos(φy))))+
//                z*(((y*z)*(1-Math.cos(φy))-(x*Math.sin(φy))));
//        double zT = x*(((x*z)*(1-Math.cos(φz))-(y*Math.sin(φz))))+
//                y*(((y*z)*(1-Math.cos(φz))+(y*Math.sin(φz))))+
//                z*(Math.cos(φz)+(Math.pow(y,2)*(1-Math.cos(φz))));
        double xT = x;
        double yT = y;
        double zT = z;
        yT = y*Math.cos(φx)-z*Math.sin(φx);
        zT = y*Math.sin(φx)+z*Math.cos(φx);
        x = xT;
        y = yT;
        z = zT;
        xT = x*Math.cos(φy)+z*Math.sin(φy);
        zT = -x*Math.sin(φy)+z*Math.cos(φy);
        x = xT;
        y = yT;
        z = zT;
        xT = x*Math.cos(φz)-y*Math.sin(φz);
        yT = -x*Math.sin(φz)+y*Math.cos(φz);
        x = xT;
        y = yT;
        z = zT;
        Log.w("Ⓜⓐⓣⓣⓔⓛ", "Ⓜⓐⓣⓣⓔⓛ        計算Ｇ值 " + xT+" , "+ yT+" , "+ zT);
        θx = Math.atan2(Math.sqrt(1-Math.pow(x,2)),x)*(180/Math.PI);
        θy = Math.atan2(Math.sqrt(1-Math.pow(y,2)),y)*(180/Math.PI);
        θz = Math.atan2(Math.sqrt(1-Math.pow(z,2)),z)*(180/Math.PI);
        Log.e("Ⓜⓐⓣⓣⓔⓛ", "Ⓜⓐⓣⓣⓔⓛ        計算角度 "  + (θx-0)%360+" , "+ (θy-0)%360+" , "+ (θz+0)%360);
    }
    @Override
    protected void onResume() {
        super.onResume();
    }

    private void startAPP() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (canUse) {
                    Intent intent = new Intent();
                    intent.setClass(getApplicationContext(), connectDeviceActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } else {
                    initPermission();
                }
            }
        }, 200);
    }

    private void initPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, PERMISSIONS_ALA_CONNECT, REQUEST_EXTERNAL_ALA_CONNECT);
        } else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, PERMISSIONS_ALA_CONNECT, REQUEST_EXTERNAL_ALA_CONNECT);
            } else {
                canUse = true;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_ALA_CONNECT:
                break;
        }
    }

    private void initScanDevice(String sharedPreferencesScanDeviceString) {
        SCAN_DEVICE.clear();
        String[] ScanList = sharedPreferencesScanDeviceString.split("_BDTS_");
        if (ScanList.length > 0) {
            for (int i = 0; i < ScanList.length; i++) {
                SCAN_DEVICE.add(ScanList[i]);
            }
        } else {
//            SCAN_DEVICE.add("inRide");
//            SCAN_DEVICE.add("SC003");
//            SCAN_DEVICE.add("Star ONE");
        }
    }

    private void settingScan() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(alabdtlibrary_MainActivity.this);
        builderSingle.setIcon(R.mipmap.icon_scan_setting);
        builderSingle.setTitle("Setting scan device…");
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(alabdtlibrary_MainActivity.this, android.R.layout.select_dialog_singlechoice);
        for (String str_device : SCAN_DEVICE) {
            arrayAdapter.add(str_device);
        }
        builderSingle.setNegativeButton(getString(R.string.Cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    AlertDialog.Builder builderInner = new AlertDialog.Builder(alabdtlibrary_MainActivity.this);
                    builderInner.setMessage("Add new scan keywords");
                    final EditText input = new EditText(alabdtlibrary_MainActivity.this);
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT);
                    input.setLayoutParams(lp);
                    builderInner.setView(input);
                    builderInner.setPositiveButton(getString(R.string.Certain), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (!input.getText().toString().trim().equals("")) {
                                ScanDeviceString += input.getText().toString() + "_BDTS_";
                                sharedPreferences.edit().putString("SCAN_DEVICE", ScanDeviceString).apply();
                                initScanDevice(ScanDeviceString);
                            }
                            dialog.dismiss();
                        }
                    });
                    builderInner.setNegativeButton(getString(R.string.Cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builderInner.show();
                } else {
                    final String mName = SCAN_DEVICE.get(which).toString();
                    AlertDialog.Builder builderInner = new AlertDialog.Builder(alabdtlibrary_MainActivity.this);
                    builderInner.setMessage("Delete this scan keywords");
                    builderInner.setTitle(mName);
                    builderInner.setPositiveButton(getString(R.string.Certain), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ScanDeviceString = ScanDeviceString.replace(mName + "_BDTS_", "");
                            sharedPreferences.edit().putString("SCAN_DEVICE", ScanDeviceString).apply();
                            initScanDevice(ScanDeviceString);
                            dialog.dismiss();
                        }
                    });
                    builderInner.setNegativeButton(getString(R.string.Cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builderInner.show();
                }
            }
        });
        builderSingle.show();
        builderSingle.setCancelable(false);
    }
}
