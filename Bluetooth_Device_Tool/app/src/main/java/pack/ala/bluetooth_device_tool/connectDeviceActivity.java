package pack.ala.bluetooth_device_tool;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import pack.ala.ala_ble.BLETestClass;
import pack.ala.common_function.BLEUse;

public class connectDeviceActivity extends AppCompatActivity {
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};

    public static BLETestClass BleTestClass;
    private BLEUse mBLEUse = new BLEUse();
    private boolean BLEClassUse = true;
    private boolean BLECanWrite = false;
    private boolean deviceConnect = false;

    private LeDeviceListAdapter mLeDeviceListAdapter;
    private final int DEF_NON_PAIR = 0;
    private final int DEF_PAIRING = 1;
    private final int DEF_PAIRED = 2;

    private final int TestScanRunnableCount = 4;
    private int TestScanCount = 0;
    private ArrayList<BLETestClass.BLEDeviceInfo> myDeviceInfoList = new ArrayList<>();

    public Activity context;
    private Handler TestDeviceHandler = new Handler();
    private Handler TestScanHandler = new Handler();

    private LinearLayout testDeviceScanList, testDeviceAction, testDeviceActionLog;
    private TextView testDeviceStop;
    private TextView sensorNameEdit, sensorUuid, sensorNumberData;
    private TextView testDeviceSend, testDeviceStatus, testDeviceDissconnect;
    private TextView testDeviceSendLog, testDeviceSendClose;
    private LinearLayout testDeviceLog;
    private ImageView testDeviceLogIcon;
    private TextView testDeviceScan, testDeviceScanStop;
    private ListView SensorListView;
    private EditText testDevicecommit_01, testDevicecommit_02, testDevicecommit_03, testDevicecommit_04, testDevicecommit_05, testDevicecommit_06,
            testDevicecommit_07, testDevicecommit_08, testDevicecommit_09, testDevicecommit_10, testDevicecommit_11, testDevicecommit_12,
            testDevicecommit_13, testDevicecommit_14, testDevicecommit_15, testDevicecommit_16, testDevicecommit_17, testDevicecommit_18;
    private ArrayList<EditText> EditArray = new ArrayList<EditText>();
    private ImageButton engineeringLogImageButton;
    public static ScrollView engineeringLogScrollView;
    public static TextView engineeringLogTextView ,btmSensorTextView;
    public static boolean textScrollDown = false;

    //-----------------------------------------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        testDeviceScanList = (LinearLayout) findViewById(R.id.testDeviceScanList);
        testDeviceAction = (LinearLayout) findViewById(R.id.testDeviceAction);
        testDeviceActionLog = (LinearLayout) findViewById(R.id.testDeviceActionLog);
        testDeviceStop = (TextView) findViewById(R.id.testDeviceStop);
        sensorNameEdit = (TextView) findViewById(R.id.sensorNameEdit);
        sensorUuid = (TextView) findViewById(R.id.sensorUuid);
        sensorNumberData = (TextView) findViewById(R.id.sensorNumberData);
        testDeviceSend = (TextView) findViewById(R.id.testDeviceSend);
        testDeviceStatus = (TextView) findViewById(R.id.testDeviceStatus);
        testDeviceStatus.setPaintFlags(testDeviceStatus.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        testDeviceDissconnect = (TextView) findViewById(R.id.testDeviceDissconnect);
        testDeviceSendLog = (TextView) findViewById(R.id.testDeviceSendLog);
        testDeviceLog = (LinearLayout) findViewById(R.id.testDeviceLog);
        testDeviceLogIcon = (ImageView) findViewById(R.id.testDeviceLogIcon);
        testDeviceSendClose = (TextView) findViewById(R.id.testDeviceSendClose);
        testDeviceScan = (TextView) findViewById(R.id.testDeviceScan);
        testDeviceScanStop = (TextView) findViewById(R.id.testDeviceScanStop);
        SensorListView = (ListView) findViewById(R.id.btmSensorList);
        btmSensorTextView = (TextView) findViewById(R.id.btmSensorTextView);
        testDevicecommit_01 = (EditText) findViewById(R.id.testDevicecommit_01);
        EditArray.add(testDevicecommit_01);
        testDevicecommit_02 = (EditText) findViewById(R.id.testDevicecommit_02);
        EditArray.add(testDevicecommit_02);
        testDevicecommit_03 = (EditText) findViewById(R.id.testDevicecommit_03);
        EditArray.add(testDevicecommit_03);
        testDevicecommit_04 = (EditText) findViewById(R.id.testDevicecommit_04);
        EditArray.add(testDevicecommit_04);
        testDevicecommit_05 = (EditText) findViewById(R.id.testDevicecommit_05);
        EditArray.add(testDevicecommit_05);
        testDevicecommit_06 = (EditText) findViewById(R.id.testDevicecommit_06);
        EditArray.add(testDevicecommit_06);
        testDevicecommit_07 = (EditText) findViewById(R.id.testDevicecommit_07);
        EditArray.add(testDevicecommit_07);
        testDevicecommit_08 = (EditText) findViewById(R.id.testDevicecommit_08);
        EditArray.add(testDevicecommit_08);
        testDevicecommit_09 = (EditText) findViewById(R.id.testDevicecommit_09);
        EditArray.add(testDevicecommit_09);
        testDevicecommit_10 = (EditText) findViewById(R.id.testDevicecommit_10);
        EditArray.add(testDevicecommit_10);
        testDevicecommit_11 = (EditText) findViewById(R.id.testDevicecommit_11);
        EditArray.add(testDevicecommit_11);
        testDevicecommit_12 = (EditText) findViewById(R.id.testDevicecommit_12);
        EditArray.add(testDevicecommit_12);
        testDevicecommit_13 = (EditText) findViewById(R.id.testDevicecommit_13);
        EditArray.add(testDevicecommit_13);
        testDevicecommit_14 = (EditText) findViewById(R.id.testDevicecommit_14);
        EditArray.add(testDevicecommit_14);
        testDevicecommit_15 = (EditText) findViewById(R.id.testDevicecommit_15);
        EditArray.add(testDevicecommit_15);
        testDevicecommit_16 = (EditText) findViewById(R.id.testDevicecommit_16);
        EditArray.add(testDevicecommit_16);
        testDevicecommit_17 = (EditText) findViewById(R.id.testDevicecommit_17);
        EditArray.add(testDevicecommit_17);
        testDevicecommit_18 = (EditText) findViewById(R.id.testDevicecommit_18);
        EditArray.add(testDevicecommit_18);
        engineeringLogScrollView = ((ScrollView) findViewById(R.id.engineeringLogScrollView));
        engineeringLogTextView = ((TextView) findViewById(R.id.engineeringLogTextView));
        engineeringLogImageButton = ((ImageButton) findViewById(R.id.engineeringLogImageButton));
        context = connectDeviceActivity.this;
        if (BleTestClass == null) {
            BleTestClass = new BLETestClass();
            BleTestClass.initactivity(connectDeviceActivity.this);
        }
        initUI();
        initListener();
        checkPermission();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (BleTestClass == null) {
            BleTestClass = new BLETestClass();
            BleTestClass.initactivity(connectDeviceActivity.this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BleTestClass.closeGattService();
    }

    @Override
    public void onBackPressed() {
//        exitTest();

    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exitTest();
        }

        return false;
    }

    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                ActivityCompat.requestPermissions(this, PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE);
            else {
                BleTestClass.initBLE();
            }
        } else {
            BleTestClass.initBLE();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    exitTest();
                }
        }
    }

    protected void initUI() {
        if (deviceConnect) {
            testDeviceScanList.setVisibility(View.GONE);
            testDeviceActionLog.setVisibility(View.VISIBLE);
        } else {
            testDeviceScanList.setVisibility(View.VISIBLE);
            testDeviceActionLog.setVisibility(View.GONE);
            sensorNameEdit.setText("");
            sensorUuid.setText("");
            sensorNumberData.setText("");
            testDeviceStatus.setText("");
        }
    }

    protected void initListener() {
        testDeviceStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exitTest();
            }
        });
        testDeviceDissconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (deviceConnect) {
                    testDeviceDisConnect();
                }
            }
        });
        testDeviceSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (deviceConnect) {
                    setEdit();
                    testDeviceAction.setVisibility(View.VISIBLE);
                }
            }
        });

        testDeviceSendLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (BleTestClass.deviceCallWrite) {
                    if (deviceConnect && checkEdit()) {
                        String[] engineeringArray = new String[20];
                        engineeringArray[0] = "0";
                        for (int i = 0; i < EditArray.size(); i++) {
                            engineeringArray[i + 1] = EditArray.get(i).getText().toString();
                        }
                        engineeringArray[19] = "0";
                        BleTestClass.engineeringwrite(engineeringArray);
                    } else {
                        Toast.makeText(connectDeviceActivity.this, getString(R.string.Toast_Input_format_1)+"\n"+getString(R.string.Toast_Input_format_2), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(connectDeviceActivity.this, getString(R.string.Toast_Bluetooth_WriteNull), Toast.LENGTH_LONG).show();
                }
                hideSoftKeyboard();
            }
        });

        testDeviceSendClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboard();
                if (deviceConnect) {
                    testDeviceAction.setVisibility(View.GONE);
                }
            }
        });

        testDeviceScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkGPS()){
                    if (BleTestClass.checkBluetooth() && !deviceConnect) {
//                    BleTestClass.scanLeDevice(true);
                        BleTestClass.mBluetoothAdapter.startLeScan(BleTestClass.mLeScanCallback);
                        myDeviceInfoList.clear();
                        mLeDeviceListAdapter = new LeDeviceListAdapter(connectDeviceActivity.this, myDeviceInfoList);
                        SensorListView.setAdapter(mLeDeviceListAdapter);
                        TestScanCount = 0;
                        testDeviceScan.setEnabled(false);
                        TestScanHandler.postDelayed(TestScanRunnable, 0);
                    }
                }
            }
        });
        testDeviceScanStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                BleTestClass.scanLeDevice(false);
                BleTestClass.mBluetoothAdapter.stopLeScan(BleTestClass.mLeScanCallback);
                TestScanCount = -1;
                testDeviceScan.setText(getString(R.string.Scan_Device));
                testDeviceScan.setEnabled(true);
            }
        });


        engineeringLogImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (textScrollDown) {
                    engineeringLogImageButton.setImageResource(R.mipmap.arrow_downup);
                    textScrollDown = false;
                } else {
                    engineeringLogImageButton.setImageResource(R.mipmap.arrow_down);
                    textScrollDown = true;
                }
            }
        });

        testDeviceLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(BleTestClass.logSaveType==0){
                    BleTestClass.logSaveType = 1;
                    BleTestClass.logSave = true;
                    testDeviceLogIcon.setImageResource(R.mipmap.icon_logsave);
                }else if(BleTestClass.logSaveType==1){
//                    BleTestClass.logSaveType = 2;
//                    BleTestClass.logSave = true;
//                    testDeviceLogIcon.setImageResource(R.mipmap.icon_logsave2);
//                    BleTestClass.sevaLogInMap();
//                }else if(BleTestClass.logSaveType==2){
                    BleTestClass.logSaveType = 0;
                    BleTestClass.logSave = false;
                    testDeviceLogIcon.setImageResource(R.mipmap.icon_logview);
                    BleTestClass.sevaLogInMap();
                }
                hideSoftKeyboard();
            }
        });
    }
    public boolean checkGPS() {
        Boolean returnGPS = false;
        String DeviceBRAND = android.os.Build.BRAND.toString();
        String[] DeviceString = { ""};
        for (String DeviceStrings : DeviceString) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && DeviceBRAND.contains(DeviceStrings) && !isOpenGps()){
                returnGPS = false;
                Toast.makeText(connectDeviceActivity.this.getApplicationContext(), getString(R.string.Toast_Open_Gps), Toast.LENGTH_SHORT).show();
                connectDeviceActivity.this.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }else{
                returnGPS = true;
            }
        }
        return returnGPS;
    }
    public boolean isOpenGps() {
        LocationManager locationManager = (LocationManager) connectDeviceActivity.this.getSystemService(Context.LOCATION_SERVICE);
        boolean gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean network = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (gps || network) {
            return true;
        }
        return false;
    }
    private void initDeviceListView() {
        myDeviceInfoList = BleTestClass.getDeviceInfoList();
        SensorListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (myDeviceInfoList.size() == 0)
                    return;

                for (int iCnt = 0; iCnt < myDeviceInfoList.size(); iCnt++)
                    myDeviceInfoList.get(iCnt).status_BTM = DEF_NON_PAIR;

                BleTestClass.GATT_UUID = "";
                BleTestClass.engineering_text = "";
                BleTestClass.deviceCallWrite = false;
                BleTestClass.mDeviceAddress = myDeviceInfoList.get(position).DeviceAddress;
                BleTestClass.mDeviceName = myDeviceInfoList.get(position).DeviceName;
                myDeviceInfoList.get(position).status_BTM = DEF_PAIRING;
                mLeDeviceListAdapter.notifyDataSetChanged();

                sensorNameEdit.setText(myDeviceInfoList.get(position).DeviceName);
                sensorNumberData.setText(BleTestClass.mDeviceAddress);

                BleTestClass.mBluetoothAdapter.stopLeScan(BleTestClass.mLeScanCallback);
                TestScanCount = -1;
                testDeviceScan.setEnabled(true);
                testDeviceScan.setText(getString(R.string.Scan_Device));
                TestScanHandler.removeCallbacks(TestScanRunnable);
                testDeviceConnect();
            }
        });
        mLeDeviceListAdapter = new LeDeviceListAdapter(this, myDeviceInfoList);
        SensorListView.setAdapter(mLeDeviceListAdapter);
    }

    private void testDeviceConnect() {
//        bindService(new Intent(this, BluetoothLeService.class), BleTestClass.mServiceConnection, BIND_AUTO_CREATE);
        registerReceiver(BleTestClass.mGattUpdateReceiver, BleTestClass.makeGattUpdateIntentFilter());
        if (BleTestClass.mBluetoothLeService != null) {
            Log.w(connectDeviceActivity.this.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ       testDeviceConnect Connect  " + BleTestClass.mDeviceAddress);
            BleTestClass.mBluetoothLeService.connect(BleTestClass.mDeviceAddress);
        } else {
            Log.w(connectDeviceActivity.this.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ       testDeviceConnect Connect");
            BleTestClass.setBindService();
        }
        deviceConnect = true;
        initUI();
        TestDeviceHandler.postDelayed(TestDeviceRunnable, 1000);
    }
    private void testDeviceDisConnect() {
        new AlertDialog.Builder(connectDeviceActivity.this, android.R.style.Theme_Material_Dialog_Alert)
                .setTitle(getString(R.string.Dissconnect)+" "+getString(R.string.Device))
                .setMessage(getString(R.string.Message_agree_Disconnect)+"\n"+BleTestClass.mDeviceName)
                .setCancelable(true)
                .setNegativeButton(getString(R.string.Certain), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (BleTestClass.logSave)BleTestClass.sevaLogInMap();
                        BleTestClass.closeGattService();
                        deviceConnect = false;
                        initUI();
                    }
                })
                .setPositiveButton(getString(R.string.Cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).show();
    }
    protected void setEdit() {
        for (int i = 0; i < EditArray.size(); i++) {
            EditArray.get(i).setText("00");
            EditArray.get(i).setInputType(android.text.InputType.TYPE_CLASS_NUMBER);
        }
    }

    protected void exitTest() {
        if (deviceConnect) {
            testDeviceDisConnect();
        }else{

        }
    }

    protected boolean checkEdit() {
        Boolean canSend = true;
        for (int i = 0; i < EditArray.size(); i++) {
            if (EditArray.get(i).getText().toString().trim().equals("")) {
                canSend = false;
            } else if (Integer.valueOf(EditArray.get(i).getText().toString()) > 255) {
                canSend = false;
            }
        }
        return canSend;
    }


    private Runnable TestScanRunnable = new Runnable() {
        @Override
        public void run() {
            if (TestScanCount > TestScanRunnableCount || TestScanCount == -1) {
                BleTestClass.mBluetoothAdapter.stopLeScan(BleTestClass.mLeScanCallback);
                TestScanCount = -1;
                testDeviceScan.setText(getString(R.string.Scan_Device));
                testDeviceScan.setEnabled(true);
                initDeviceListView();
            } else {
                testDeviceScan.setText(getString(R.string.Scan)+"(" + (int) (TestScanRunnableCount - TestScanCount) + ")");
                TestScanCount++;
                TestScanHandler.removeCallbacks(TestScanRunnable);
                TestScanHandler.postDelayed(TestScanRunnable, 1000);
            }

        }
    };
    private final Runnable TestDeviceRunnable = new Runnable() {
        public void run() {
            if (deviceConnect) {
                testDeviceStatus.setText(BleTestClass.actionReceiver);
                sensorUuid.setText((BleTestClass.GATT_UUID.length() > 5) ? BleTestClass.GATT_UUID.split("-")[0] : "");
                TestDeviceHandler.removeCallbacks(TestDeviceRunnable);
                TestDeviceHandler.postDelayed(TestDeviceRunnable, 1000);
            } else {
                testDeviceStatus.setText(getString(R.string.Unconnected));
                TestDeviceHandler.removeCallbacks(TestDeviceRunnable);
            }
        }
    };


    //-----------------------------------------------------------------------------------------------
    private class LeDeviceListAdapter extends BaseAdapter {
        private LayoutInflater myInflater;
        ArrayList<BLETestClass.BLEDeviceInfo> myDeviceInfoList;

        public LeDeviceListAdapter(Context context, ArrayList<BLETestClass.BLEDeviceInfo> myDeviceInfoList) {
            myInflater = LayoutInflater.from(context);
            this.myDeviceInfoList = myDeviceInfoList;
        }

        @Override
        public int getCount() {
            if (myDeviceInfoList.size() == 0)
                return 1;
            else
                return myDeviceInfoList.size();
        }

        @Override
        public Object getItem(int position) {
            return myDeviceInfoList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @SuppressLint("ViewHolder")
        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = myInflater.inflate(R.layout.item_sensor, viewGroup, false);

            TextView nameView = (TextView) view.findViewById(R.id.nameText);
            TextView addressView = (TextView) view.findViewById(R.id.addressText);
            ImageView strengthView = (ImageView) view.findViewById(R.id.strengthView);
            ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
            ImageView markView = (ImageView) view.findViewById(R.id.markView);

            if (myDeviceInfoList.size() == 0) {
                nameView.setText("Searching");
                addressView.setText("");
                strengthView.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                markView.setVisibility(View.INVISIBLE);
            } else {
                nameView.setText(myDeviceInfoList.get(i).DeviceName);
                if (i != 0 || i != (myDeviceInfoList.size() - 1)) {
                    if (myDeviceInfoList.get(i).DeviceName.contains(MainActivity.addHRMsensor)) {
                        nameView.setTextColor(getResources().getColor(R.color.springgreen));
                    } else  if (myDeviceInfoList.get(i).DeviceName.contains(MainActivity.addOD007)) {
                        nameView.setTextColor(getResources().getColor(R.color.mediumorchid));
                    } else  if (myDeviceInfoList.get(i).DeviceName.contains(MainActivity.addStarONE)) {
                        nameView.setTextColor(getResources().getColor(R.color.accent));
                    } else  if (myDeviceInfoList.get(i).DeviceName.contains("null")) {
                        nameView.setTextColor(getResources().getColor(R.color.transparentBlackA));
                    } else {
                        nameView.setTextColor(getResources().getColor(R.color.deepskyblue));
                    }
                }

                addressView.setText(myDeviceInfoList.get(i).DeviceAddress);
                strengthView.setVisibility(View.VISIBLE);
                if (myDeviceInfoList.get(i).status_BTM != 0) {
                    Log.i(connectDeviceActivity.this.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        ConnectTo  " + myDeviceInfoList.get(i).DeviceAddress + " →→ " + myDeviceInfoList.get(i).status_BTM);
                }

                switch (myDeviceInfoList.get(i).status_BTM) {
                    case DEF_NON_PAIR:
                        progressBar.setVisibility(View.INVISIBLE);
                        markView.setVisibility(View.INVISIBLE);
                        break;

                    case DEF_PAIRING:
                        progressBar.setVisibility(View.VISIBLE);
                        markView.setVisibility(View.INVISIBLE);
                        break;

                    case DEF_PAIRED:
                        progressBar.setVisibility(View.INVISIBLE);
                        markView.setVisibility(View.VISIBLE);
                        break;
                }

                switch (mBLEUse.get_signal_Level(myDeviceInfoList.get(i).DeviceRSSI)) {
                    case 0:
                        strengthView.setImageResource(R.mipmap.ble_0);
                        break;
                    case 1:
                        strengthView.setImageResource(R.mipmap.ble_1);
                        break;
                    case 2:
                        strengthView.setImageResource(R.mipmap.ble_2);
                        break;
                    case 3:
                        strengthView.setImageResource(R.mipmap.ble_3);
                        break;
                    case 4:
                        strengthView.setImageResource(R.mipmap.ble_4);
                        break;
                    case 5:
                        strengthView.setImageResource(R.mipmap.ble_5);
                        break;
                }
            }

            if (myDeviceInfoList.size() == 0 || myDeviceInfoList.size() == 1)
                view.setBackgroundResource(R.drawable.table_one_item_selector);
            else {
                if (i == 0)
                    view.setBackgroundResource(R.drawable.table_top_selector);
                else if (i == (myDeviceInfoList.size() - 1)) {
                    view.setBackgroundResource(R.drawable.table_bottom_selector);
                } else {
                    view.setBackgroundResource(R.drawable.table_between_selector);
                }
            }

            return view;
        }
    }
    private void hideSoftKeyboard(){
        InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
        if(getCurrentFocus()!=null && getCurrentFocus().getWindowToken()!=null){
            imm.hideSoftInputFromWindow(connectDeviceActivity.this.getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
