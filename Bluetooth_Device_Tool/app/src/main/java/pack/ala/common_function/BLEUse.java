package pack.ala.common_function;

public class BLEUse //extends View 
{
	public BLEUse()
	{
 
    }
//----------------------------------------------------------------------------------------	

	public int get_signal_Level(int rssi)//使用RSSI 卡 訊號的 LEVEL
    {
        if (rssi>-60)
        {return 5;}
        if (rssi>-70)
        {return 4;}
        if (rssi>-80)
        {return 3;}
        if (rssi>-90)
        {return 2;}
        if (rssi>-110)
        {return 1;}
        return 0;
    }   
//----------------------------------------------------------------------------------------	
	
	public int get_battery_Level(int u8_battery_energy)
    {
    	if(u8_battery_energy>=100)
    	{return 4;}
    	if(u8_battery_energy>=95)
    	{return 3;}	
    	if(u8_battery_energy>=85)
    	{return 2;}
    	if(u8_battery_energy>=75)
    	{return 1;}
    	return 0;	
    }
//----------------------------------------------------------------------------------------	
	
}
