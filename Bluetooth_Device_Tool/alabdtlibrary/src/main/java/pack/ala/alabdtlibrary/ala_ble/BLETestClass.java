package pack.ala.alabdtlibrary.ala_ble;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.text.Html;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import pack.ala.alabdtlibrary.R;
import pack.ala.alabdtlibrary.bluetooth_device_tool.alabdtlibrary_MainActivity;
import pack.ala.alabdtlibrary.bluetooth_device_tool.connectDeviceActivity;

public class BLETestClass {
    public static final int REQUEST_ENABLE_BT = 1;
    private BluetoothGattCharacteristic notify_characteristic;
    private BluetoothGattCharacteristic write_characteristic;
    public BluetoothAlaService mBluetoothLeService;
    public BluetoothAdapter mBluetoothAdapter;
    private Activity activity;
    public Handler testDeviceBleHandler = new Handler();
    private int testDeviceBleRunnableCount = 0;
    private byte rolling = 0x00;
    private String action = BluetoothAlaService.ACTION_GATT_DISCONNECTED;
    public String GATT_UUID = "";
    public String engineering_text = "";
    public String mDeviceAddress = "";
    public String mDeviceName = "";
    public String actionReceiver = "Dissconnect";
    public boolean deviceCallBack = false;
    public boolean deviceCallWrite = false;
    public boolean showSelectDialog = true;
    public boolean disconnectAction = false;

    public File fileLog,fileObj;
    public String logESFileName = "";
    public StringBuilder fileLogdata;
    public BufferedReader reader = null;
    public FileOutputStream osw = null;
    public BufferedReader readerLog = null;
    public FileOutputStream oswLog = null;
    public boolean logSave = false;
    public int logSaveType = 0;
    public int logCount = 0;
    public int ObjCount = 0;
    public int tempRolling = 777;
    public int totalRolling = 0;
    public int totalData = 0;
    public String logFileName = "";
    public String logAddString = "";
    public String logSaveString = "";
    public String ObjAddString = "";

    public static class BLEDeviceInfo {
        public int status_BTM;
        public String DeviceName;
        public String DeviceAddress;
        public int DeviceRSSI;
    }

    public List<String> blelist;
    public List<BluetoothGattService> deviceGattServices;
    public ArrayList<BLEDeviceInfo> DeviceInfoList = new ArrayList<>();

    public ArrayList<BLEDeviceInfo> getDeviceInfoList() {
        return DeviceInfoList;
    }

    public final ArrayList<String> TEST_PICK_DEVICE = new ArrayList<String>() {
        private static final long serialVersionUID = 1L;

        {
            add(alabdtlibrary_MainActivity.addHRMsensor);
//            add(alabdtlibrary_MainActivity.addStarONE);
//            add(alabdtlibrary_MainActivity.addOD007);
        }
    };

    public BLETestClass() {
        blelist = new ArrayList<>();
    }

    //Handler removeCallbacks and postDelayed
    public void postHandle(Handler mHandler, Runnable mRunnable, int mDelayed) {
        mHandler.removeCallbacks(mRunnable);
        if (mDelayed >= 0) {
            mHandler.postDelayed(mRunnable, mDelayed);
        }
    }

    //BLUETOOTH 確認藍芽開關
    public boolean checkBluetooth() {
        Boolean returnBLE = true;
        if (mBluetoothAdapter == null) {
            Toast.makeText(activity, activity.getString(R.string.Toast_Bluetooth_not_supported), Toast.LENGTH_SHORT).show();
            returnBLE = false;
        }
        if (!mBluetoothAdapter.isEnabled()) {
            returnBLE = false;
            activity.startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), REQUEST_ENABLE_BT);
        }
        return returnBLE;
    }

    //BLUETOOTH 手機藍芽關閉
    public static void turnOffBluetooth() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter != null) {
            bluetoothAdapter.disable();
        }
    }

    //藍芽搜尋回傳
    public BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi, byte[] scanRecord) {
            if(alabdtlibrary_MainActivity.ScanDeviceString.contains("ALL")){
                if (device.getName()==null){
                    if(alabdtlibrary_MainActivity.ScanDeviceString.contains("null")){
                        BLEDeviceInfo myBLEDeviceInfo = new BLEDeviceInfo();
                        myBLEDeviceInfo.DeviceName = "null";
                        myBLEDeviceInfo.DeviceAddress = device.getAddress();
                        myBLEDeviceInfo.DeviceRSSI = rssi;
                        updateDeviceInfoList(myBLEDeviceInfo);
                    }
                }else{
                    BLEDeviceInfo myBLEDeviceInfo = new BLEDeviceInfo();
                    myBLEDeviceInfo.DeviceName = device.getName();
                    myBLEDeviceInfo.DeviceAddress = device.getAddress();
                    myBLEDeviceInfo.DeviceRSSI = rssi;
                    updateDeviceInfoList(myBLEDeviceInfo);
                }
            }else if(alabdtlibrary_MainActivity.ScanDeviceString.contains(alabdtlibrary_MainActivity.adddefault)){
                for (String str_device : TEST_PICK_DEVICE) {
                    if (device.getName()==null){
                        if(alabdtlibrary_MainActivity.ScanDeviceString.contains("null")){
                            BLEDeviceInfo myBLEDeviceInfo = new BLEDeviceInfo();
                            myBLEDeviceInfo.DeviceName = "null";
                            myBLEDeviceInfo.DeviceAddress = device.getAddress();
                            myBLEDeviceInfo.DeviceRSSI = rssi;
                            updateDeviceInfoList(myBLEDeviceInfo);
                        }
                    }else if (device.getName().contains(str_device)) {
                        BLEDeviceInfo myBLEDeviceInfo = new BLEDeviceInfo();
                        myBLEDeviceInfo.DeviceName = device.getName();
                        myBLEDeviceInfo.DeviceAddress = device.getAddress();
                        myBLEDeviceInfo.DeviceRSSI = rssi;
                        updateDeviceInfoList(myBLEDeviceInfo);
                    }
                }
            }else{
                //            if (device == null || device.getAddress() == null || device.getName() == null) return;
                for (String str_device : alabdtlibrary_MainActivity.SCAN_DEVICE) {
                    if (device.getName()==null){
                        if(alabdtlibrary_MainActivity.ScanDeviceString.contains("null")){
                            BLEDeviceInfo myBLEDeviceInfo = new BLEDeviceInfo();
                            myBLEDeviceInfo.DeviceName = "null";
                            myBLEDeviceInfo.DeviceAddress = device.getAddress();
                            myBLEDeviceInfo.DeviceRSSI = rssi;
                            updateDeviceInfoList(myBLEDeviceInfo);
                        }
                    }else if (device.getName().contains(str_device)) {
                        BLEDeviceInfo myBLEDeviceInfo = new BLEDeviceInfo();
                        myBLEDeviceInfo.DeviceName = device.getName();
                        myBLEDeviceInfo.DeviceAddress = device.getAddress();
                        myBLEDeviceInfo.DeviceRSSI = rssi;
                        updateDeviceInfoList(myBLEDeviceInfo);
                    }
                }
            }
        }
    };

    //搜尋回傳顯示
    private void updateDeviceInfoList(BLEDeviceInfo BLEDeviceInfo) {
        boolean b_DeviceExist = false;
        for (int iCnt = 0; iCnt < DeviceInfoList.size(); iCnt++)
            if (DeviceInfoList.get(iCnt).DeviceAddress.equals(BLEDeviceInfo.DeviceAddress)) {
                b_DeviceExist = true;
                BLEDeviceInfo.status_BTM = DeviceInfoList.get(iCnt).status_BTM;
                DeviceInfoList.set(iCnt, BLEDeviceInfo);
                break;
            }
        if (!b_DeviceExist) {
            BLEDeviceInfo.status_BTM = 0;
            DeviceInfoList.add(BLEDeviceInfo);
        }
    }

    //BLUETOOTH 傳輸 Action Type
    public static IntentFilter makeGattUpdateIntentFilter() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothAlaService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothAlaService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothAlaService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothAlaService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        intentFilter.addAction(BluetoothDevice.ACTION_PAIRING_REQUEST);
        return intentFilter;
    }

    //BLUETOOTH ServiceConnection
    public final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            Log.w(activity.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        ServiceConnection onServiceConnected");
            mBluetoothLeService = ((BluetoothAlaService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
            }
            mBluetoothLeService.connect(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.w(activity.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        ServiceConnection onServiceDisconnected");
            mBluetoothLeService = null;
        }
    };

    //ACTIVITY 重整
    public void initactivity(Activity activity) {
        this.activity = activity;
        Log.i(activity.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        initactivity");
    }

    //DEVICE 重整
    public void initBLE() {
        Log.i(activity.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        initBLE");
        if (!activity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(activity, activity.getString(R.string.Toast_Bluetooth_not_supported), Toast.LENGTH_SHORT).show();
            activity.finish();
        }
        mBluetoothAdapter = ((BluetoothManager) activity.getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(activity, activity.getString(R.string.Toast_Bluetooth_not_supported), Toast.LENGTH_SHORT).show();
            activity.finish();
        }
    }

    //BLUETOOTH Service Handler
    public Runnable testDeviceBleRunnable = new Runnable() {
        @Override
        public void run() {
            if (deviceCallBack) {
                Log.v(activity.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        testDeviceBleRunnable  deviceCallBack");
                if(totalRolling!=-1){
                    totalData++;
                    if(totalData<totalRolling)totalData = totalRolling;
                    if(totalData%2==0){
                        connectDeviceActivity.btmSensorTextView.setText("應收："+(int)(totalData/2)+"  實收："+(int)(totalRolling/2)+"\nPER："+(100-(totalRolling*100/totalData))+" %");
                    }
                }
            } else {
                connectDeviceActivity.btmSensorTextView.setText("");
                Log.e(activity.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        testDeviceBleRunnable  Not deviceCallBack");
                if (testDeviceBleRunnableCount == 8) {
                    try {
                        retureGATTclose();
                    } catch (Exception e) {
                        Log.e(activity.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        testDeviceBleRunnable  catch");
                        e.printStackTrace();
                    }
                    testDeviceBleRunnableCount = 0;
                } else {
                    testDeviceBleRunnableCount++;
                }

            }
            postHandle(testDeviceBleHandler, testDeviceBleRunnable, 1000);
        }
    };

    //BLUETOOTH Service 裝置連線
    public void setBindService() {
        Log.w(activity.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        setBindService");
        if (testDeviceBleRunnable != null)
            postHandle(testDeviceBleHandler, testDeviceBleRunnable, -1);
        activity.getApplicationContext().bindService(new Intent(activity, BluetoothAlaService.class), mServiceConnection, activity.BIND_AUTO_CREATE);
        activity.getApplicationContext().registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        postHandle(testDeviceBleHandler, testDeviceBleRunnable, 1000);
    }

    //BLUETOOTH Service 裝置重連
    public void reBindService() {
        Log.w(activity.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        reBindService");
        if (testDeviceBleRunnable != null)
            postHandle(testDeviceBleHandler, testDeviceBleRunnable, -1);
        activity.getApplicationContext().bindService(new Intent(activity, BluetoothAlaService.class), mServiceConnection, activity.BIND_AUTO_CREATE);
        activity.getApplicationContext().registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        postHandle(testDeviceBleHandler, testDeviceBleRunnable, 1000);
    }

    //BLUETOOTH Service 選擇連線 Uuid
    public void selectGattServices(final List<BluetoothGattService> gattServices) {
        Log.w(activity.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        ⓢelectGattServices  GATT_UUID  " + GATT_UUID);
        if (GATT_UUID.equals("")) {
            AlertDialog.Builder builderSingle = new AlertDialog.Builder(activity);
            builderSingle.setIcon(R.mipmap.bt_send_to_device);
            builderSingle.setTitle("Select Connect Services:");
            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(activity, android.R.layout.select_dialog_singlechoice);
            for (BluetoothGattService gattService : gattServices) {
                arrayAdapter.add(gattService.getUuid().toString());
            }
            builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    final String strName = arrayAdapter.getItem(which);
                    GATT_UUID = strName;
                    displayGattServices(gattServices);
                    dialog.dismiss();
//                AlertDialog.Builder builderInner = new AlertDialog.Builder(activity);
//                builderInner.setMessage(strName);
//                builderInner.setTitle("Your Selected Item is");
//                builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog,int which) {
//                        GATT_UUID = strName;
//                        displayGattServices(gattServices);
//                        dialog.dismiss();
//                    }
//                });
//                builderInner.show();
                }
            });
            builderSingle.show();
            builderSingle.setCancelable(false);
        } else {
            displayGattServices(gattServices);
        }
    }

    //BLUETOOTH Service 建立連線 Uuid
    private void displayGattServices(List<BluetoothGattService> gattServices) {
        Log.w(activity.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        ⓓisplayGattServices  GATT_UUID  " + GATT_UUID);
        if (gattServices == null) {
            Log.e(activity.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        ⓓisplayGattServices  gattServices == null  ");
            return;
        }
        if (GATT_UUID.equals("")) {
            selectGattServices(gattServices);
        } else {
            for (BluetoothGattService gattService : gattServices) {
                if (GATT_UUID.equals(gattService.getUuid().toString())) {
//                    BluetoothLeService.UUID_TESTDEVICE = gattService.getUuid();
                    Log.i(activity.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        gattService.uuid  " + gattService.getUuid());
                    List<BluetoothGattCharacteristic> gattCharacteristics = gattService.getCharacteristics();
                    for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                        Log.d(activity.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        getProperties  " + gattCharacteristic.getDescriptor(gattCharacteristic.getUuid()));
                        Log.d(activity.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        getProperties  " + gattCharacteristic.getProperties());
                        if (gattCharacteristic.getProperties() == BluetoothGattCharacteristic.PROPERTY_NOTIFY) {
                            Log.e(activity.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        gattService.uuid notify_characteristic  " + gattCharacteristic.getUuid());
                            notify_characteristic = gattCharacteristic;
                            mBluetoothLeService.setCharacteristicNotification(gattCharacteristic, true);
                            mBluetoothLeService.readCharacteristic(notify_characteristic);
                        } else if (gattCharacteristic.getProperties() == BluetoothGattCharacteristic.PROPERTY_WRITE ||
                                gattCharacteristic.getProperties() == BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE ||
                                gattCharacteristic.getProperties() == (int) (BluetoothGattCharacteristic.PROPERTY_WRITE + BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE)) {
                            Log.e(activity.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        gattService.uuid write_characteristic  " + gattCharacteristic.getUuid());
                            write_characteristic = gattCharacteristic;
                            deviceCallWrite = true;
                        }
                    }
                }
            }
        }
    }

    //BLUETOOTH Service action
    public final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            action = intent.getAction();
            Log.d(activity.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        action  " + action);
            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                actionReceiver = activity.getString(R.string.Waiting)+" "+activity.getString(R.string.Connect);
                if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1)
                        == BluetoothAdapter.STATE_OFF) {
                } else if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1)
                        == BluetoothAdapter.STATE_ON) {
                }
            }
            if (BluetoothAlaService.ACTION_GATT_CONNECTED.equals(action)) {
                actionReceiver = activity.getString(R.string.Build)+" "+activity.getString(R.string.Connect);
                setTextToEngineeringLog("<font color=#FFFFFF> "+actionReceiver+"</font>" + "<br>");
                deviceCallBack = true;
                showSelectDialog = true;
                totalRolling = -1;
                totalData = 1;
                checkFilePath();
                if(mDeviceName.contains(alabdtlibrary_MainActivity.addHRMsensor)){
                    connectDeviceActivity.btmSensorTextView.setVisibility(ScrollView.VISIBLE);
                }else{
                    connectDeviceActivity.btmSensorTextView.setVisibility(ScrollView.GONE);
                }
            } else if (BluetoothAlaService.ACTION_GATT_DISCONNECTED.equals(action)) {
                setTextToEngineeringLog("<font color=#FF0000> "+actionReceiver+"</font>" + "<br>");
                actionReceiver = activity.getString(R.string.Dissconnect);
                deviceCallBack = false;
                testDeviceBleRunnableCount = 0;
                if(disconnectAction){
                    disconnectAction = false;
                    mBluetoothLeService.close();
                    activity.getApplicationContext().unregisterReceiver(mGattUpdateReceiver);
                    mBluetoothLeService = null;
                }
            } else if (BluetoothAlaService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                actionReceiver = activity.getString(R.string.Waiting)+" "+activity.getString(R.string.Send);
                setTextToEngineeringLog("<font color=#ff55ff> "+actionReceiver+"</font>" + "<br>");
                if (showSelectDialog) {
                    if (GATT_UUID.equals("")) {
                        if (deviceGattServices != null) deviceGattServices.clear();
                        deviceGattServices = mBluetoothLeService.getSupportedGattServices();
                    }
                    selectGattServices(deviceGattServices);
                    showSelectDialog = false;
                }
            } else if (BluetoothAlaService.ACTION_DATA_AVAILABLE.equals(action)) {
                actionReceiver = activity.getString(R.string.Certain)+" "+activity.getString(R.string.Send);
                deviceCallBack = true;
                try {
                    if(totalRolling==-1)totalRolling=0;
                    getData(intent.getStringExtra(BluetoothAlaService.EXTRA_DATA));
                } catch (Exception e) {
//                    sendLogToServer("✖✖✖✖✖ 藍芽回應失敗");
                    e.printStackTrace();
                }
            } else if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
                actionReceiver = activity.getString(R.string.Enter)+" "+activity.getString(R.string.Pair);
                switch (((BluetoothDevice) intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE)).getBondState()) {
                    case BluetoothDevice.BOND_BONDING:
                        Log.e("BlueToothTestActivity", "pairing......");
                        break;
                    case BluetoothDevice.BOND_BONDED:
                        break;
                    case BluetoothDevice.BOND_NONE:
                        break;
                    default:
                        break;
                }
            } else if (BluetoothDevice.ACTION_PAIRING_REQUEST.equals(action)) {
                actionReceiver = activity.getString(R.string.Waiting)+" "+activity.getString(R.string.Pair);
                BluetoothDevice btDevice = intent
                        .getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                try {
                    btDevice.getClass().getMethod("setPairingConfirmation", boolean.class).invoke(btDevice, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };

    //BLUETOOTH Service 回傳資料
    private void getData(String str_data) {
        int nowRolling = Integer.valueOf(str_data.split(",")[0], 16);
        Log.i(activity.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        getData " + str_data+"   nowRolling "+nowRolling);
        if (str_data != null && tempRolling!=nowRolling) { // && nowRolling!=tempRolling
            if(logSave){
                if(mDeviceName.equals("SC003")){
                    if(logCount==0)sevaLogInString("index,X,Y,Z");
                    if(logSaveType == 1){
                        sevaLogInString(initSC003Data(str_data));
                    }else if(logSaveType == 2){
                        sevaLogInString(initSC003Data2(str_data));
                    }
                }else if(mDeviceName.contains("OB0")){
                    String[] strings = str_data.split(",");
                    if(logCount==0)sevaLogInString("rolling,Comand,gsensor X,gsensor Y,gsensor Z,step ,cadence");
                    String OB_Data = "\n"+nowRolling+","+strings[1]+","
                            +((Integer.valueOf(strings[3]+strings[2], 16).intValue()>(65535/2))?(Integer.valueOf(strings[3]+strings[2], 16).intValue()-65535):(Integer.valueOf(strings[3]+strings[2], 16).intValue()))+","
                            +((Integer.valueOf(strings[5]+strings[4], 16).intValue()>(65535/2))?(Integer.valueOf(strings[5]+strings[4], 16).intValue()-65535):(Integer.valueOf(strings[5]+strings[4], 16).intValue()))+","
                            +((Integer.valueOf(strings[7]+strings[6], 16).intValue()>(65535/2))?(Integer.valueOf(strings[7]+strings[6], 16).intValue()-65535):(Integer.valueOf(strings[7]+strings[6], 16).intValue()))+","
                            +Integer.valueOf(strings[9]+strings[8], 16).intValue()+","+Integer.valueOf(strings[10], 16);
                    sevaLogInString(OB_Data);
                }else{
                    sevaLogInString("\n"+String.format("%03d",(int)(logCount+1))+"  "+str_data);
                }
            }else{
                setTextToEngineeringLog("<font color=#3aa33a> "+activity.getString(R.string.Receive)+"： " + str_data + "</font>" + "<br>");
            }
            tempRolling = nowRolling;
            totalRolling++;
        }
    }

    //BLUETOOTH Service 斷線
    public void disconnect() {
        Log.w(activity.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        disconnect");
        if (mBluetoothLeService != null) {
            mBluetoothLeService.disconnect();
            mBluetoothLeService.close();
        }
    }

    //BLUETOOTH Service 斷線註消
    public void nullReceiver() {
        Log.w(activity.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        nullReceiver");
        if (mBluetoothLeService != null) {
            disconnectAction = true;
            mBluetoothLeService.disconnect();
        }
    }

    //BLUETOOTH Service 重新建立連線
    public void returnGattService() {
        setBindService();
    }

    //BLUETOOTH Gatt 關閉
    public void closeService() {
        Log.e(activity.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        closeService");
        try {
            if (mServiceConnection != null)
                activity.getApplicationContext().unbindService(mServiceConnection);
//            unregisterReceiver();
        } catch (Exception e) {

        }
        postHandle(testDeviceBleHandler, testDeviceBleRunnable, -1);
    }

    //BLUETOOTH Gatt 關閉清空
    public void closeGattService() {
        Log.e(activity.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        closeGattService");
        try {
            if (mServiceConnection != null)
                activity.getApplicationContext().unbindService(mServiceConnection);
            nullReceiver();
        } catch (Exception e) {

        }
        postHandle(testDeviceBleHandler, testDeviceBleRunnable, -1);
    }

    //BLUETOOTH GATT 重新建立連線前切斷GATT
    public void retureGATTclose() {
        closeService();
        new GATTcloseAsyncTask().execute();
    }

    //BLUETOOTH GATT 重新建立連線前等待
    class GATTcloseAsyncTask extends AsyncTask<Integer, Void, String> {
        @Override
        protected String doInBackground(Integer... page) {
            try {
                Thread.sleep(2000);
                retureGATT();
            } catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
        }
    }

    //BLUETOOTH GATT 重新建立連線
    public void retureGATT() {
        Log.w(activity.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        retureGATT");
        reBindService();
    }

    //輸入模式
    private void setTextToEngineeringLog(String str) {
        try {
            connectDeviceActivity.engineeringLogTextView.setText(Html.fromHtml(getEngineeringLogText(str)), TextView.BufferType.SPANNABLE);
            if (connectDeviceActivity.textScrollDown)
                connectDeviceActivity.engineeringLogScrollView.fullScroll(ScrollView.FOCUS_DOWN);
        } catch (Exception e) {
        }
    }

    public String getEngineeringLogText(String LogText) {
        String[] logS = engineering_text.split("<br>");
        if (logS.length >= 30) {
            engineering_text = "";
            for (int i = 5; i < logS.length; i++) {
                engineering_text += logS[i] + "<br>";
            }
        } else {
            engineering_text += LogText;
        }
        return engineering_text;
    }

    //輸入模式 刷新
    public void engineeringwrite(String[] stage) {
        if (write_characteristic == null) {
            setTextToEngineeringLog("<font color=#FFAA00>"+activity.getString(R.string.Toast_Bluetooth_WriteNull)+"</font>" + "<br>");
        } else {
            byte[] send = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
            send[0] = (byte) (rolling++ % 0x100);
            for (int i = 1; i < stage.length; i++) {
                int sendInt = Integer.valueOf(stage[i]);
                send[i] = (byte) sendInt;
            }
            send[19] = (byte) (send[1] ^ send[2] ^ send[3] ^ send[4] ^ send[5] ^ send[6] ^ send[7] ^ send[8] ^ send[9] ^ send[10] ^ send[11] ^ send[12] ^ send[13] ^ send[14] ^ send[15] ^ send[16] ^ send[17] ^ send[18]);
            String s = "";
            for (int i = 0; i < send.length; i++)
                s += send[i] + ",";
            Log.d(activity.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        engineeringwrite  " + s);
            setTextToEngineeringLog("<font color=#00FFFF>" + s + "</font>" + "<br>");
            write_characteristic.setValue(send);
            writeCharacteristic(mBluetoothLeService, write_characteristic);
        }
    }

    //輸入模式 發送
    private Boolean writeCharacteristic(BluetoothAlaService BluetoothService, BluetoothGattCharacteristic write_characteristic) {
        Boolean writeResult = false;
        try {
            writeResult = BluetoothService.mBluetoothGatt.writeCharacteristic(write_characteristic);
        } catch (Exception e) {
            Toast.makeText(activity, activity.getString(R.string.Toast_Bluetooth_Writefail), Toast.LENGTH_LONG).show();
        }
        Log.w(activity.getPackageName(), "ⒷⓁⓊⒺⓉⓄⓄⓉⒽ        writeCharacteristic " + writeResult);
        return writeResult;
    }

    //連線日誌
    private void checkFilePath(){
        fileLog = new File(alabdtlibrary_MainActivity.FILE_ALA_BDT + "log/");
        if (!fileLog.exists()) {
            fileLog.mkdirs();
        }
        String nowTime = Calendar.getInstance().get(Calendar.YEAR) + "" + String.format("%02d",(int)(Calendar.getInstance().get(Calendar.MONTH)+1)) + ""  + String.format("%02d",Calendar.getInstance().get(Calendar.DAY_OF_MONTH))+
                String.format("%02d",Calendar.getInstance().get(Calendar.HOUR_OF_DAY)) + ""  + String.format("%02d",Calendar.getInstance().get(Calendar.MINUTE)) + "" + String.format("%02d",Calendar.getInstance().get(Calendar.SECOND)) + "";
        logFileName = mDeviceName+"_"+nowTime+".csv";
        fileLog = new File(fileLog, logFileName);
        fileObj = new File(alabdtlibrary_MainActivity.FILE_ALA_ES);
        if (!fileObj.exists()) {
            fileObj.mkdirs();
        }
        logESFileName = mDeviceName+"_"+nowTime+".obj";
        fileObj = new File(fileObj, logESFileName);

        logAddString = "";
        ObjAddString = "";
        logCount = 0;
        ObjCount = 0;
    }
    public void sevaLogInString(String logString) {
        if(logCount%50==0){
            setTextToEngineeringLog("<font color=#00bfff> "+activity.getString(R.string.Collect)+" " +activity.getString(R.string.Logs) + "("+logCount+")" + "</font>" +"<br>");
        }
        logAddString+=logString;
        logCount++;
        if(logCount>=500){
            sevaLogInMap();
        }
    }
    public void sevaLogInMap() {
        setTextToEngineeringLog("<font color=#7ffd4> "+activity.getString(R.string.Save)+" " +activity.getString(R.string.Logs) + "("+logCount+")：<br>" +
                alabdtlibrary_MainActivity.FILE_ALA_BDT + "log/" + logFileName + "</font>" + "<br>");
        logSaveString = logAddString;
        logAddString = "";
        logCount = 0;
        new sevaLogAsyncTask().execute();
    }

    class sevaLogAsyncTask extends AsyncTask<Integer, Void, String> {
        @Override
        protected String doInBackground(Integer... integers) {
            try {
                oswLog = new FileOutputStream(fileLog);
                oswLog.write(logSaveString.getBytes());
                oswLog.flush();
                oswLog.close();
            }catch (Exception e){
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            checkFilePath();
        }
    }
    private String initSC003Data(String s){
        String[] strings = s.split(",");
        int rollingCount = (Integer.valueOf(strings[0], 16).byteValue()<0)?(int)(256+Integer.valueOf(strings[0], 16).byteValue()):Integer.valueOf(strings[0], 16).byteValue();
        return
                "\n"+rollingCount+"(1),"
                        +((Integer.valueOf(strings[2]+strings[1], 16).intValue()>(65535/2))?(Integer.valueOf(strings[2]+strings[1], 16).intValue()-65535):(Integer.valueOf(strings[2]+strings[1], 16).intValue()))+","
                        +((Integer.valueOf(strings[4]+strings[3], 16).intValue()>(65535/2))?(Integer.valueOf(strings[4]+strings[3], 16).intValue()-65535):(Integer.valueOf(strings[4]+strings[3], 16).intValue()))+","
                        +((Integer.valueOf(strings[6]+strings[5], 16).intValue()>(65535/2))?(Integer.valueOf(strings[6]+strings[5], 16).intValue()-65535):(Integer.valueOf(strings[6]+strings[5], 16).intValue()))+","
                +"\n"+rollingCount+"(2),"
                        +((Integer.valueOf(strings[8]+strings[7], 16).intValue()>(65535/2))?(Integer.valueOf(strings[8]+strings[7], 16).intValue()-65535):(Integer.valueOf(strings[8]+strings[7], 16).intValue()))+","
                        +((Integer.valueOf(strings[10]+strings[9], 16).intValue()>(65535/2))?(Integer.valueOf(strings[10]+strings[9], 16).intValue()-65535):(Integer.valueOf(strings[10]+strings[9], 16).intValue()))+","
                        +((Integer.valueOf(strings[12]+strings[11], 16).intValue()>(65535/2))?(Integer.valueOf(strings[12]+strings[11], 16).intValue()-65535):(Integer.valueOf(strings[12]+strings[11], 16).intValue()))+","
                +"\n"+rollingCount+"(3),"
                        +((Integer.valueOf(strings[14]+strings[13], 16).intValue()>(65535/2))?(Integer.valueOf(strings[14]+strings[13], 16).intValue()-65535):(Integer.valueOf(strings[14]+strings[13], 16).intValue()))+","
                        +((Integer.valueOf(strings[16]+strings[15], 16).intValue()>(65535/2))?(Integer.valueOf(strings[16]+strings[15], 16).intValue()-65535):(Integer.valueOf(strings[16]+strings[15], 16).intValue()))+","
                        +((Integer.valueOf(strings[18]+strings[17], 16).intValue()>(65535/2))?(Integer.valueOf(strings[18]+strings[17], 16).intValue()-65535):(Integer.valueOf(strings[18]+strings[17], 16).intValue()));
    }
    private String initSC003Data2(String s){
        String[] strings = s.split(",");
        int rollingCount = (Integer.valueOf(strings[0], 16).byteValue()<0)?(int)(256+Integer.valueOf(strings[0], 16).byteValue()):Integer.valueOf(strings[0], 16).byteValue();
        return
                "\n"+rollingCount+"(1),"
                        +((Integer.valueOf(strings[2]+strings[1], 16).intValue()>(65535/2))?(Integer.valueOf(strings[2]+strings[1], 16).intValue()-65535):(Integer.valueOf(strings[2]+strings[1], 16).intValue()))+","
                        +"\n"+rollingCount+"(2),"
                        +((Integer.valueOf(strings[4]+strings[3], 16).intValue()>(65535/2))?(Integer.valueOf(strings[4]+strings[3], 16).intValue()-65535):(Integer.valueOf(strings[4]+strings[3], 16).intValue()))+","
                        +"\n"+rollingCount+"(3),"
                        +((Integer.valueOf(strings[6]+strings[5], 16).intValue()>(65535/2))?(Integer.valueOf(strings[6]+strings[5], 16).intValue()-65535):(Integer.valueOf(strings[6]+strings[5], 16).intValue()))+","
                        +"\n"+rollingCount+"(4),"
                        +((Integer.valueOf(strings[8]+strings[7], 16).intValue()>(65535/2))?(Integer.valueOf(strings[8]+strings[7], 16).intValue()-65535):(Integer.valueOf(strings[8]+strings[7], 16).intValue()))+","
                        +"\n"+rollingCount+"(5),"
                        +((Integer.valueOf(strings[10]+strings[9], 16).intValue()>(65535/2))?(Integer.valueOf(strings[10]+strings[9], 16).intValue()-65535):(Integer.valueOf(strings[10]+strings[9], 16).intValue()))+","
                        +"\n"+rollingCount+"(6),"
                        +((Integer.valueOf(strings[12]+strings[11], 16).intValue()>(65535/2))?(Integer.valueOf(strings[12]+strings[11], 16).intValue()-65535):(Integer.valueOf(strings[12]+strings[11], 16).intValue()))+","
                        +"\n"+rollingCount+"(7),"
                        +((Integer.valueOf(strings[14]+strings[13], 16).intValue()>(65535/2))?(Integer.valueOf(strings[14]+strings[13], 16).intValue()-65535):(Integer.valueOf(strings[14]+strings[13], 16).intValue()))+","
                        +"\n"+rollingCount+"(8),"
                        +((Integer.valueOf(strings[16]+strings[15], 16).intValue()>(65535/2))?(Integer.valueOf(strings[16]+strings[15], 16).intValue()-65535):(Integer.valueOf(strings[16]+strings[15], 16).intValue()))+","
                        +"\n"+rollingCount+"(9),"
                        +((Integer.valueOf(strings[18]+strings[17], 16).intValue()>(65535/2))?(Integer.valueOf(strings[18]+strings[17], 16).intValue()-65535):(Integer.valueOf(strings[18]+strings[17], 16).intValue()));
    }
}
